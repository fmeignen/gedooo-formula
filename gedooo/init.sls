# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'gedooo/map.jinja' import gedooo with context %}

gedooo:
  pkg.installed:
     - name: unzip

/etc/init.d/tomcat:
  file.managed:
    - source: salt://gedooo/tomcat
    - hash: md5=f0a507abc061b62e5f76e6a1d3bf017d
    - mode: 750


{% for cfg in ('tomcat6', 'ooo') %}
/etc/default/{{ cfg }}:
  file.managed:
    - source: salt://gedooo/{{ cfg }}

{% endfor %}

gedooo_dist:
  archive.extracted:
    - name: /tmp/ODF-asalae
    - source: {{ gedooo.dist.url }}
    - archive_format: tar
    - source_hash: {{ gedooo.dist.hash }}
    - keep: True  # XXX
    - require:
      - pkg: unzip
  
tomcat-archive:
  archive.extracted:
    - name: /opt
    - source: "/tmp/ODF-asalae/apache-tomcat-6.0.35.tgz"
    - archive_format: tar
    - source_hash: md5=e7de6261acddceaad74cbc32bb50bea8
    - keep: True    
    - if_missing: /opt/apache-tomcat-6.0.35


testuser:
  user.absent

    
/opt/apache-tomcat-6.0.35:
 file.directory:
 - user: tomcat
 - group: tomcat
 - recurse:
   - user
   - group
 - require:
      - user: tomcat

/opt/tomcat:
  file.symlink:
    - target: /opt/apache-tomcat-6.0.35
    - user: tomcat
    - group: tomcat
    - require:
      - user: tomcat

tomcat:
  group:
    - present
  user:
    - present
    - fullname: tomcat gedooo
    - shell: /bin/bash
    - groups:
      - tomcat
    - require:
      - group: tomcat
  service.running:
    - enable: True
    - reload: True

